// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import VueAMap from 'vue-amap'

Vue.config.productionTip = false
Vue.use(ElementUI)


Vue.use(VueAMap)
VueAMap.initAMapApiLoader({
  key: 'd3cd713567086808acb58e14895d9da2',
  v: '1.4.15'
});
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
