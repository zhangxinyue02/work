import request from '@/router/axios';
import { baseUrl } from '../../../config/index';
//列表数据
export const getList = (data) => request({
    url: baseUrl + '/sch/news/findPage',
    method: 'get',
    params: data,
});
//保存
export const getSave = (obj) => request({
    url: baseUrl + '/sch/news/save',
    method: 'post',
    data: obj,
});
//编辑保存
export const getEditSave = (obj) => request({
    url: baseUrl + '/sch/news/update',
    method: 'post',
    data: obj,
});
//编辑
export const getEdit = (id) => request({
    url: baseUrl + '/sch/news/detail?id=' + id,
    method: 'get',
});
//删除
export const getDelete = (id) => request({
    url: baseUrl + '/sch/news/delete?id=' + id,
    method: 'get',
});