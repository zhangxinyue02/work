import request from '@/router/axios';
import { baseUrl } from '../../../config/index';
//列表数据
export const getList = (data) => request({
    url: baseUrl + '/sch/news/findPage',
    method: 'get',
    params: data,
});