/**
 * 全站http配置
 *
 * axios参数说明
 * isSerialize是否开启form表单提交
 * isToken是否需要token
 */
import axios from 'axios'
import {
    Message,
} from 'element-ui'



axios.defaults.timeout = 60000;
//返回其他状态吗
axios.defaults.validateStatus = function(status) {
    return status >= 200 && status <= 500; // 默认的
};
//跨域请求，允许保存cookie
axios.defaults.withCredentials = true;
// NProgress Configuration
// NProgress.configure({
//     showSpinner: false
// });
// 请求拦截器
axios.interceptors.request.use(
    config => {
        // 每次发送请求之前判断是否存在token，如果存在，则统一在http请求的header都加上token，不用每次请求都手动添加了
        // 即使本地存在token，也有可能token是过期的，所以在响应拦截器中要对返回状态进行判断
        const token = window.localStorage.getItem("token");
        token && (config.headers.Authorization = token);
        // showFullScreenLoading()
        // setTimeout(function() {

        //     tryHideFullScreenLoading() 

        // }, 40000);

        return config;
    },
    error => {
        return Promise.error(error);
    })

// 响应拦截器
axios.interceptors.response.use(
    response => {
        if (response.status === 200) {
            // tryHideFullScreenLoading()
            return Promise.resolve(response);
        } else {
            switch (response.status) {
                // 401: 未登录                
                // 未登录则跳转登录页面，并携带当前页面的路径                
                // 在登录成功后返回当前页面，这一步需要在登录页操作。                
                case 401:
                    router.replace({
                        path: '../views/login',
                        query: { redirect: router.currentRoute.fullPath }
                    });
                    break;
                    // 403 token过期                
                    // 登录过期对用户进行提示                
                    // 清除本地token和清空vuex中token对象                
                    // 跳转登录页面                
                case 403:
                    Message({
                            message: '登录过期，请重新登录',
                            type: 'error',
                            duration: 5 * 1000
                        })
                        // 清除token                    
                    localStorage.removeItem('token');
                    // 跳转登录页面，并将要浏览的页面fullPath传过去，登录成功后跳转需要访问的页面
                    setTimeout(() => {
                        router.replace({
                            path: '../views/login',
                        });
                    }, 1000);
                    break;
                    // 404请求不存在                
                case 404:
                    Message({
                        message: '网络请求不存在',
                        type: 'error',
                        duration: 5 * 1000
                    })
                    break;
                    // 其他错误，直接抛出错误提示                
                default:
                    Message({
                        message: '服务器异常',
                        type: 'error',
                        duration: 5 * 1000
                    })

            }
            return Promise.reject(response);
        }
    },
    // 服务器状态码不是200的情况    
    error => {
        Message({
                message: '链接超时',
                type: 'error',
            })
            // window.reload()
        return Promise.reject(error.response);
    }
);
export default axios;