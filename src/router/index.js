import Vue from 'vue'
import VueRouter from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Home from '@/web/home'

// import TopMenu from '@/components/topMenu'
import News from '@/web/news'
import GameCenter from '@/web/gameCenter'
import CartoonCenter from '@/web/CartoonCenter'
import MoviesCenter from '@/web/moviesCenter'
import Business from '@/web/business'
import AboutUs from '@/web/aboutUs'
import CopyrightCenter from '@/web/copyrightCenter'
import Contact from '@/web/contactUs'
import Map from '@/web/map'
import NewsAdmin from '@/backStage/newsAdmin'




Vue.use(VueRouter)

const originalPush = VueRouter.prototype.push

VueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}

export default new VueRouter({
    routes: [{
            path: '/index',
            name: 'HelloWorld',
            component: HelloWorld
        },
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/gameCenter',
            name: 'gameCenter',
            component: GameCenter
        },
        {
            path: '/cartoonCenter',
            name: 'cartoonCenter',
            component: CartoonCenter
        },
        {
            path: '/moviesCenter',
            name: 'moviesCenter',
            component: MoviesCenter
        },
        {
            path: '/business',
            name: 'business',
            component: Business
        },
        {
            path: '/aboutUs',
            name: 'aboutUs',
            component: AboutUs
        },
        {
            path: '/copyrightCenter',
            name: 'copyrightCenter',
            component: CopyrightCenter
        },
        {
            path: '/news',
            name: 'news',
            component: News
        },
        {
            path: '/contactUs',
            name: 'contactUs',
            component: Contact
        },
        {
            path: '/newsAdmin',
            name: 'NewsAdmin',
            component: NewsAdmin
        },
        {
            path: '/map',
            name: 'map',
            component: Map
        },
        
    ]
})